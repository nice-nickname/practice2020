﻿using System;
using System.Collections.Generic;

abstract class Animal
{
	public string TypeName { get; set; }
	public abstract void Roar();
}

class Penguin : Animal
{
	public override void Roar()
	{
		Console.WriteLine("Я " + TypeName + ". Пингвинячий писк");
	}

	public Penguin()
	{
		TypeName = "Пингвин";
	}
}

class Cow : Animal
{
	public override void Roar()
	{
		Console.WriteLine("Я " + TypeName + ". МуууууУуУУУУуууУ");
	}

	public Cow()
	{
		TypeName = "Корова";
	}
}

class SchoolVisiter : Animal
{
	public override void Roar()
	{
		Console.WriteLine("Я " + TypeName + ". Я делаю разметку на HTML и CSS");
	}

	public SchoolVisiter()
	{
		TypeName = "Школьник";
	}
}

class Boozer : Animal
{
	public override void Roar()
	{
		Console.WriteLine("Я " + TypeName + ". Я пьян");
	}

	public Boozer()
	{
		TypeName = "Алкаш";
	}
}

class Zoo
{
	public List<Animal> z;

	public void Add(Animal a)
	{
		z.Add(a);
	}

	public void RoarQueue()
	{
		foreach (var animal in z)
		{
			animal.Roar();
		}
	}

	public Zoo()
	{
		z = new List<Animal>();
	}
}

class Program
{
	static void Main(string[] args)
	{
		Zoo Z = new Zoo();

		Z.Add(new Penguin());
		Z.Add(new Cow());
		Z.Add(new Boozer());
		Z.Add(new SchoolVisiter());

		Z.RoarQueue();
	}
}
